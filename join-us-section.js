/* eslint-disable class-methods-use-this */
// eslint-disable-next-line max-classes-per-file
const instance = null;

class Section {
  constructor(headlineText, subscribeText) {
    this.headlineText = headlineText;
    this.subscribeText = subscribeText;
    if (!instance) {
      window.addEventListener('load', () => {
        const joinOurProgramSection = document.createElement('section');
        joinOurProgramSection.className = 'app-section app-section--image-program';
        const main = document.getElementById('app-container');
        main.appendChild(joinOurProgramSection);
        main.insertBefore(joinOurProgramSection, main.children[4]);
        const headline = document.createElement('h1');
        headline.className = 'app-title--ourprogram';
        headline.innerText = headlineText;
        joinOurProgramSection.appendChild(headline);
        const smlHeadline = document.createElement('h2');
        smlHeadline.className = 'app-smlTitle';
        smlHeadline.innerText = `Sed do eiusmod tempor incididunt 
                ut labore et dolore magna aliqua.`;
        joinOurProgramSection.appendChild(smlHeadline);
        const divFlex = document.createElement('div');
        divFlex.className = 'input-div-flex';
        joinOurProgramSection.appendChild(divFlex);
        const email = document.createElement('input');
        email.setAttribute('type', 'email');
        email.setAttribute('placeholder', 'Email');
        email.className = 'app-section--email';
        divFlex.appendChild(email);
        const subscribe = document.createElement('button');
        subscribe.innerHTML = subscribeText;
        subscribe.className = 'app-section--subscribe';
        divFlex.appendChild(subscribe);

        const emailStorage = localStorage.getItem('emailValue');
        if (email.length > 0) {
          email.value = emailStorage;
        }

        subscribe.addEventListener('click', (event) => {
          event.preventDefault();
          // eslint-disable-next-line no-console
          console.log(event);
        }, false);
      });
    }
  }
}

// eslint-disable-next-line import/prefer-default-export
export class SectionCreator {
  // eslint-disable-next-line class-methods-use-this
  // eslint-disable-next-line consistent-return
  create(type) {
    // eslint-disable-next-line default-case
    switch (type) {
      case 'standard':
        return new Section('Join Our Program', 'SUBSCRIBE');
      case 'advanced':
        return new Section('Join Our Advanced Program', 'Subscribe to Advanced Program');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  remove() {
    setTimeout(() => {
      const mainTag = document.getElementById('app-container');
      const removeMainTag = mainTag.children[4];
      removeMainTag.remove();
    }, 2000);
  }
}
