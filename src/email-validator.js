// eslint-disable-next-line import/prefer-default-export
export const validate = setTimeout(() => {
  const main = document.getElementById('app-container');
  const btn = main.children[4].children[2].children[1];
  const input = main.children[4].children[2].children[0];
  const container = main.children[4].children[2];

  btn.addEventListener('click', () => {
    const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
    const email = input.value;
    const end = email.substring(email.indexOf('@') + 1);
    const start = email.substring(0, email.indexOf('@'));

    // eslint-disable-next-line no-mixed-operators
    if (end === VALID_EMAIL_ENDINGS[0] && start.length > 0
        // eslint-disable-next-line no-mixed-operators
        || end === VALID_EMAIL_ENDINGS[1] && start.length > 0
        // eslint-disable-next-line no-mixed-operators
        || end === VALID_EMAIL_ENDINGS[2] && start.length > 0) {
      localStorage.setItem('emailValue', email);
      // eslint-disable-next-line eqeqeq
      if (btn.innerHTML == 'SUBSCRIBE') {
        input.style.display = 'none';
        btn.style.height = '42px';
        btn.innerHTML = 'UNSUBSCRIBE';
        container.style.justifyContent = 'center';
      } else {
        input.style.display = 'inline-block';
        btn.style.height = '40px';
        btn.innerHTML = 'SUBSCRIBE';
        container.style.justifyContent = 'space-between';
        localStorage.removeItem('emailValue');
        input.value = '';
      }
    } else {
      // eslint-disable-next-line no-alert
      alert('false');
    }
    const emailNew = document.querySelector('app-section--subscribe');
    emailNew.style.display = 'none';
  }, false);
}, 1000);
