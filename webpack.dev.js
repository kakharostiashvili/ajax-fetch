const { merge } = require('webpack-merge'); // eslint-disable-line
const common = require('./webpack.config'); // eslint-disable-line

module.exports = merge (common, { // eslint-disable-line
    mode: 'development', // eslint-disable-line
});
